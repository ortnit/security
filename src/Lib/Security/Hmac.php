<?php
/**
 * Created by PhpStorm.
 * User: tobi
 * Date: 13.12.17
 * Time: 23:59
 * https://csrc.nist.gov/publications/detail/fips/198/1/final
 */

namespace Ortnit\Lib\Security;


class Hmac
{
    protected $_algorithm = 'sha1';
    protected $_message = '';
    protected $_key = '';

    protected $_algorithms = [
        'sha1' => 64,
        'md5' => 64,
        'sha256' => 64
    ];

    public function __construct()
    {
    }

    public function setMessage($message)
    {
        $this->_message = $message;
    }

    public function setKey($key)
    {
        $this->_key = $key;
    }

    public function setAlgorithm($algorithm)
    {
        if (array_key_exists($algorithm, $this->_algorithms) !== false) {
            $this->_algorithm = $algorithm;
        }
    }

    /**
     * @return string
     */
    public function getHmac($raw = false)
    {
        $blockLength = $this->_algorithms[$this->_algorithm];

        $ipad = str_repeat("\x36", $blockLength);
        $opad = str_repeat("\x5c", $blockLength);

        $key = $this->_key;
        if (strlen($key) > $blockLength) {
            $key = $this->_hash($key);
        }
        $key = str_pad($key, $blockLength, "\0");

        //$this->_testMessage($opad);

        $key_ipad = $key ^ $ipad;
        $key_opad = $key ^ $opad;

        $hash = $this->_hash($key_opad . $this->_hash($key_ipad . $this->_message));

        if (!$raw) {
            $hash = $this->_textToHex($hash);
        }

        return $hash;
    }

    protected function _hash($message)
    {
        return hash($this->_algorithm, $message, true);
        //return hash('sha256', $message);
    }

    protected function _testMessage($message)
    {
        foreach (str_split($message) as $index => $char) {
            echo $index . ": " . $char . " => " . ord($char) . " (0x" . dechex(ord($char)) . ")\n";
        }
    }

    /**
     * @param String $text
     * @return String
     */
    protected function _textToHex(String $text): String
    {
        $hexList = [];
        foreach (str_split($text) as $char) {
            $hexList[] = sprintf('%02s', dechex(ord($char)));
        }

        return implode('', $hexList);
    }
}
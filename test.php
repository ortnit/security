<?php
/**
 * Created by PhpStorm.
 * User: tobi
 * Date: 13.12.17
 * Time: 23:46
 */
require 'vendor/autoload.php';

use Ortnit\Lib\Security\Hmac;



$key = md5('the key for this test');
$message = 'das ist eine test message um hmac zu testen';
$algorithm = 'sha256';

echo "key: " . $key . "\n";
echo "message: " . $message . "\n";
echo "algorithm: " . $algorithm . "\n";

$digest1 = hash_hmac($algorithm, $message, $key, false);

$hmac = new Hmac();
$hmac->setMessage($message);
$hmac->setKey($key);
$hmac->setAlgorithm($algorithm);
$digest2 = $hmac->getHmac();

echo "digest: " . $digest1 . "\n";
echo "digest: " . $digest2 . "\n";

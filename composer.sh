#!/usr/bin/env bash

curl -sS https://getcomposer.org/installer | php
chmod +x composer.phar
./composer.phar self-update
./composer.phar update
